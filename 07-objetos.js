
// clase 
// funcion constructora
/**
	id: id del div donde quiero dibujar
**/

let Cursor=function(id){
	// propiedad privada
	let objeto=document.querySelector(id);

	let numero=0;
	
	objeto.addEventListener("click",(e) => {
		// crear un div sin dibujarle
		let elemento=document.createElement("div");
		elemento.setAttribute("class","cursor");
		elemento.style.left=e.offsetX + "px";	  
		elemento.style.top=e.offsetY + "px";
		// añada el div creado en el objeto lienzo
		objeto.appendChild(elemento);
		numero++;
	});
	
	this.getNumeroPuntos=function(){
		return numero;
	}	
}


// creando un objeto
let uno=new Cursor("#lienzo");

// creando otro objeto
let dos=new Cursor("#lienzo1");


