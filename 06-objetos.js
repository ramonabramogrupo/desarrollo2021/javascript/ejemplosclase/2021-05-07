// funcion constructora
// clase
let Caja=function(an=0,al=0){
	// propiedades
	// privadas
	let ancho=0;
	let alto=0;

	// getter para mis propiedades
	this.getAncho=function() {
		return ancho + "px";
	}

	this.getAlto=function() {
		return alto + "px";
	}

	// setter para mis propiedades
	this.setAncho=function(valor){
			ancho=valor;
	}

	this.setAlto=function(valor){
			alto=valor;
	}


	// metodo publico
	this.dibujar=function(){
		return `<div style="width:${this.getAncho()};height:${this.getAlto()};background-color:#ccc"> </div>`
	}

	// metodo constructor
	this.Caja=function(an,al){
		this.setAncho(an);
		this.setAlto(al);
	}

	this.Caja(an,al);
}

let caja1=new Caja(23,45);
document.write(caja1.dibujar());
