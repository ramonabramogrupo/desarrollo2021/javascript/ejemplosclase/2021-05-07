// funcion constructora
// Clase Caja
// es una plantilla para crear nuevos objetos
let Caja=function(ancho=0,alto=0){
	// miembros
	// propiedades
	// publicas
	this.ancho=ancho;
	this.alto=alto;
}

// funcion constructora
// Clase Coche
// plantilla o molde para crear nuevos coches
let Coche=function(valores={}){
	// miembros
	// propiedades
	// publicas
	this.marca=valores.hasOwnProperty("marca")?valores.marca:"";
	this.modelo=valores.hasOwnProperty("modelo")?valores.modelo:"";
}

// creando objetos
let caja1=new Caja();

console.log(caja1.ancho); // 0
console.log(caja1.alto); // 0

// creando objeto
let caja2=new Caja(23,45);
console.log(caja2.ancho); // 23
console.log(caja2.alto); // 45

// creando objeto
let caja3=new Caja(20);
console.log(caja3.ancho); // 20
console.log(caja3.alto); // 0

// creando coche
let coche1=new Coche({
	marca:"ford"
});

console.log(coche1.marca);  // ford
console.log(coche1.modelo);  // ""

// creando coche
let coche2=new Coche();

console.log(coche2.marca);  // ""
console.log(coche2.modelo);  // ""


//creando coche
let coche3=new Coche({
	marca:"Fiat",
	modelo:"Tipo"
});

console.log(coche3.marca);  // "Fiat"
console.log(coche3.modelo);  // "Tipo"

// creando coche
let coche4=new Coche({
	modelo:"Tipo"
});

console.log(coche4.marca);  // ""
console.log(coche4.modelo);  // "Tipo"








